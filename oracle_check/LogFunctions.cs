﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;

namespace oracle_check
{
    class LogFunctions
    {
        private string f_dt;
        private string f_lt;

        public string format_datetime
        {
            get
            {
                return f_dt = (DateTime.Now.ToString("yyyyMMddHHmmss"));
            }
            set
            {
                f_dt = value;
            }
        }

        public string format_logtime
        {
            get
            {
                return f_lt = (DateTime.Now.ToString("HH:mm:ss"));
            }
            set
            {
                f_lt = value;
            }
        }

    }
}
