﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;
using System.Data;

namespace oracle_check
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] code = { "INFO", "WARNING", "ERROR" };
            Settings config = new Settings(@".\etc\oracle_settings.json");
            Oracle oracle = config.server.oracle;
            bool result = new Boolean();

            ProgramFunctions pf = new ProgramFunctions(config.server.debug.flag);
            
            OracleConnection oracle_conn = pf.GetOracleConnection(oracle);
            if (oracle_conn == null)
            {
                Console.WriteLine("ERROR: Oracle Connection Object could not be created.");
                Environment.Exit(2);
            }
            OracleCommand oracle_cmd = pf.GetOracleCommand(oracle_conn);
            if (oracle_cmd == null)
            {
                Console.WriteLine("ERROR: Oracle Command Object could not be created.");
                Environment.Exit(2);
            }
            oracle_conn = pf.OpenOracleConnection(oracle_conn);
            if (oracle_conn == null)
            {
                Console.WriteLine("ERROR: Oracle Connection could not be opened.");
                Environment.Exit(2);
            }
            if (oracle_conn != null && oracle_conn.State == ConnectionState.Open)
            {
                result = pf.ExecuteOracleQuery(oracle_cmd, oracle);
                oracle_conn = pf.CloseOracleConnection(oracle_conn);
                if (result)
                {
                    Console.WriteLine("Oracle SQL Query Succeeded.");
                    Environment.Exit(0);
                }
                else 
                {
                    Console.WriteLine("ERROR: Oracle SQL Query Failed.");
                    Environment.Exit(2);
                }
            }

            if (config.server.debug.flag)
            {
                Console.ReadLine();
            }
        }
    }
}
