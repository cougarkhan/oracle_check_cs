﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.Net.Mail;
using System.Data;

namespace oracle_check
{
    class ProgramFunctions
    {
        bool DEBUG = false;
        LogFunctions lf = new LogFunctions();
        string[] code = { "INFO", "WARNING", "ERROR" };

        public ProgramFunctions()
        {
        }

        public ProgramFunctions(bool debug_flag)
        {
            DEBUG = debug_flag;
        }
        
        public OracleConnection GetOracleConnection(Oracle server)
        {
            string conn_string = server.conn_string;

            try
            {

                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Creating Oracle Connection Object.", lf.format_logtime, code[0]); }
                OracleConnection conn = new OracleConnection(conn_string);
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Created Oracle Connection Object Successfully.", lf.format_logtime, code[0]); }
                return conn;
            }
            catch (Exception e)
            {
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Create Oracle Connection Object Failed.", lf.format_logtime, code[2]); }
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}{2}", lf.format_logtime, code[2], e.ToString()); }
                return null;
            }
        }

        public OracleCommand GetOracleCommand(OracleConnection conn)
        {
            try
            {
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Creating Oracle Command Object.", lf.format_logtime, code[0]); }
                OracleCommand cmd = conn.CreateCommand();
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Created Oracle Command Object Successfully.", lf.format_logtime, code[0]); }
                return cmd;
            }
            catch (Exception e)
            {
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Create Oracle Command Object Failed.", lf.format_logtime, code[2]); }
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}{2}", lf.format_logtime, code[2], e.ToString()); }
                return null;
            }
        }

        public OracleConnection OpenOracleConnection(OracleConnection conn)
        {
            try
            {
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Opening Oracle Connection Object.", lf.format_logtime, code[0]); }
                conn.Open();
                if (conn.State == ConnectionState.Open)
                {
                    if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Oracle Connection Object State is {2}.", lf.format_logtime, code[0], conn.State); }
                    if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Opened Oracle Connection Object Successfully.", lf.format_logtime, code[0]); }
                    return conn;
                }
                else
                {
                    if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Oracle Connection Object State is {2}.", lf.format_logtime, code[0], conn.State); }
                    return null;
                }
            }
            catch (Exception e)
            {
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Open Oracle Connection Object Failed.", lf.format_logtime, code[2]); }
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}{2}", lf.format_logtime, code[2], e.ToString()); }
                return null;
            }

        }

        public OracleConnection CloseOracleConnection(OracleConnection conn)
        {
            try
            {
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Closing Oracle Connection Object.", lf.format_logtime, code[0]); }
                conn.Dispose();
                if (conn.State == ConnectionState.Closed)
                {
                    if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Oracle Connection Object State is {2}.", lf.format_logtime, code[0], conn.State); }
                    if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Closed Oracle Connection Object Successfully.", lf.format_logtime, code[0]); }
                    return conn;
                }
                else
                {
                    if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Oracle Connection Object State is {2}.", lf.format_logtime, code[0], conn.State); }
                    return null;
                }
            }
            catch (Exception e)
            {
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Close Oracle Connection Object Failed.", lf.format_logtime, code[2]); }
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}{2}", lf.format_logtime, code[2], e.ToString()); }
                return null;
            }
        }

        public bool ExecuteOracleQuery(OracleCommand cmd, Oracle server)
        {
            try
            {
                cmd.CommandText = server.sql_query;
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Executing SQL Query.", lf.format_logtime, code[0]); }
                OracleDataReader reader = cmd.ExecuteReader();

                List<string> rows = new List<string>();
                while (reader.Read())
                {
                    rows.Add(reader[0].ToString());
                }

                if (rows.Count > 0)
                {
                    if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Executed SQL Query Successfully.", lf.format_logtime, code[0]); }
                    return true;
                }
                else
                {
                    if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Execute SQL Query Failed.", lf.format_logtime, code[0]); }
                    return false;
                }
            }
            catch (Exception e)
            {
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}Execute SQL Query Failed.", lf.format_logtime, code[2]); }
                if (DEBUG) { Console.WriteLine("{0,-12}{1,-12}{2}", lf.format_logtime, code[2], e.ToString()); }
                return false;
            }
        }

    }
}
